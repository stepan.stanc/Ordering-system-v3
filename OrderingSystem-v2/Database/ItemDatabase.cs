using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using OrderingSystem_v2.Models;
using OrderingSystem_v2.Utils;
using OrderingSystem_v2.WebClient;

namespace OrderingSystem_v2.Database
{
	public class ItemDatabase
	{
		// SQLite connection
		private SQLiteAsyncConnection database;
		private Sync sync = new Sync();

		public ItemDatabase(string dbPath)
		{
			database = new SQLiteAsyncConnection(dbPath);

			database.CreateTableAsync<order>();
			database.CreateTableAsync<order_products>();
			database.CreateTableAsync<product>();
			database.CreateTableAsync<user>();
			database.CreateTableAsync<deleted>();

		}

		// Query
		public Task<List<T>> GetItemsAsync<T>() where T : ITable, new()
		{
			return database.Table<T>().ToListAsync();
		}

		// sql dotaz iniverzální v typu výsledku a v hodnotě i sloupci
		public Task<List<T>> GetItemsByColumnValue<T>(int id, string tableRow) where T : ITable, new()
		{
			string pom = String.Format("SELECT * FROM {2} WHERE {1} = {0}", id, tableRow, typeof(T).Name);
			return database.QueryAsync<T>(pom);
		}

		/// <summary>
		/// Získá poslední použité ID léku
		/// </summary>
		/// <returns></returns>
		public Task<List<T>> GetLastID<T>() where T : ITable, new()
		{
			string pom = String.Format("SELECT * FROM {0} WHERE ID = (SELECT MAX(ID) FROM {0})", typeof(T).Name);
			return database.QueryAsync<T>(pom);
		}
		//SELECT [seq] FROM [sqlite_sequence] WHERE [name] = 'Medication'

		// Query using LINQ
		public Task<T> GetItemAsync<T>(int id) where T : ITable, new()
		{
			return database.Table<T>().Where(i => i.id == id).FirstOrDefaultAsync();
		}

		public async Task<int> SaveItemAsync<T>(T item, bool saveOnly) where T : ITable, new()
		{
			if (item.id == 0 || saveOnly)
			{

				if (item.id == 0)
				{
					List<T> maxID = new List<T>();
					maxID = await GetMaxId<T>();
					if (maxID != null && maxID.Count > 0) { item.id = maxID[0].id + 1; }
				}

				return await database.InsertAsync(saveOnly ? item : item.TimeStampIn());
			}
			else
			{
				return await database.UpdateAsync(item.TimeStampUp());
			}
		}

		public async Task SaveItemsAsync<T>(List<T> items, bool saveOnly) where T : ITable, new()
		{
			foreach (T item in items)
			{
				await SaveItemAsync<T>(item, saveOnly);
			}
		}


		public Task<int> DeleteItemAsync<T>(T item) where T : ITable, new()
		{
			return database.DeleteAsync(item);
		}

		public async void DeleteItemsAsync<T>(List<T> items) where T : ITable, new()
		{
			foreach (T item in items)
			{
				await database.DeleteAsync(item);
			}
		}

		public Task<List<int>> DeleteItemAsyncByID<T>(int id) where T : ITable, new()
		{
			var tablaName = typeof(T).Name;
			LogDelete(tablaName, id);

			string pom = String.Format("DELETE FROM '{0}' WHERE ID = {1}", tablaName, id);
			return database.QueryAsync<int>(pom);
		}

		public async void DeleteItemsAsyncByID<T>(List<int> ids) where T : ITable, new()
		{
			foreach (int id in ids)
			{
				await DeleteItemAsyncByID<T>(id);
			}
		}


		public async void LogDelete(string table, int id)
		{
			string pom = String.Format("INSERT INTO deleted (item_id, table_name, timestamp) VALUES ({0}, '{1}', '{2}')", id, table, DateTime.Now.ToPHPDatetime());
			await database.QueryAsync<deleted>(pom);
		}

		public async Task<bool> IsDeleted<T>(int id) where T : ITable, new()
		{
			var del = new deleted();
			del = await database.Table<deleted>().Where(i => i.id == id && i.table_name == typeof(T).Name).FirstAsync();
			return del.id == id;
		}

		public Task<List<user>> Login(user usr)
		{
			return database.Table<user>().Where(u => u.username == usr.username && u.password == usr.password).ToListAsync();
		}

		public async Task<List<order>> UsrOrders(int usr_id)
		{
			return await database.Table<order>().Where(o => o.user_id == usr_id).ToListAsync();
		}

		public async void DeleteOrderProducts(List<int> ids)
		{
			foreach (int id in ids)
			{				
				string query = String.Format("DELETE FROM order_products WHERE order_id = {0}", id);
				string query1 = String.Format("SELECT * FROM order_products WHERE order_id = {0}", id);
				var deleted = await database.QueryAsync<order_products>(query1);
				await database.QueryAsync<int>(query);	
				foreach(order_products d in deleted)
				{
					LogDelete("order_products", d.id);
				}				
			}
		}

		public async void DeleteProductOrderProducts(List<int> ids)
		{
			foreach (int id in ids)
			{
				string query = String.Format("DELETE FROM order_products WHERE product_id = {0}", id);
				await database.QueryAsync<int>(query);
			}
		}

		public async Task<List<ViewProduct>> GetOrderProducts(int id)
		{
			string query = String.Format("SELECT product.id, product.name, product.price, product.description, order_products.count FROM product LEFT JOIN order_products ON order_products.order_id = {0} WHERE order_products.product_id = product.id", id);
			return await database.QueryAsync<ViewProduct>(query);
		}

    public async Task<List<order_products>> GetOrderProductsOnly(int id)
    {
      string query = String.Format("SELECT order_products.id,order_products.order_id,order_products.product_id,order_products.count FROM order_products WHERE order_products.order_id = {0}", id);
      return await database.QueryAsync<order_products>(query);
    }

    public async Task ClearDB()
		{
			await database.QueryAsync<int>("DELETE FROM 'order'");
			await database.QueryAsync<int>("DELETE FROM order_products");
			await database.QueryAsync<int>("DELETE FROM product");
			await database.QueryAsync<int>("DELETE FROM 'user'");
		}

		public Task<List<user>> GetUsers(int id)
		{
			string query = String.Format("SELECT * FROM user WHERE id = {0}", id);
			return database.QueryAsync<user>(query);
		}

		public Task<List<T>> GetNotSyncedItems<T>() where T : ITable, new()
		{
			string query = String.Format("SELECT * FROM '{0}' WHERE synced = '0'", typeof(T).Name, false);
			return database.QueryAsync<T>(query);
		}

		public async Task<List<T>> GetMaxId<T>() where T : ITable, new()
		{
			try
			{
				string query = String.Format("SELECT * FROM '{0}' GROUP BY id ORDER BY id DESC LIMIT 1", typeof(T).Name);
				return await database.QueryAsync<T>(query);
			}
			catch
			{
				return await Task<List<T>>.Run(() => new List<T>());
			}
		}

	}
}
