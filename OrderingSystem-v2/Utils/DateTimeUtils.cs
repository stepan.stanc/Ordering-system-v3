using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace OrderingSystem_v2.Utils
{
  public static class DateTimeUtils
  {
    public static string ToPHPDatetime(this DateTime dateT)
    {
      return dateT.ToString("yyyy-MM-dd hh:mm:ss");
    }

    public static DateTime FromPHPDatetime(this string dateT)
    {
      return string.IsNullOrWhiteSpace(dateT) || dateT == "0000-00-00 00:00:00" ? new DateTime() : DateTime.ParseExact(dateT, "yyyy-MM-dd hh:mm:ss", CultureInfo.InvariantCulture);
    }
  }
}
