using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using ServiceStack;
using ServiceStack.Text;
using OrderingSystem_v2.Models;
using OrderingSystem_v2.Database;

namespace OrderingSystem_v2.WebClient
{
	public class Rest
	{
		RestClient client = new RestClient("https://student.sps-prosek.cz/~stancst14/api_ordering_system/api.php");

		public List<user> Login(user user)
		{
			return SendNewPost<List<user>>("log-in", user);
		}

		public bool CheckUsernameUsed(user user)
		{
			return SendNewPost<List<user>>("user-by-name", user).Count >= 1;
		}

		public List<user> GetUsers()
		{
			return SendNewGet<List<user>>("get-all-usrs");
		}

		public List<deleted> GetDeleted()
		{
			return SendNewGet<List<deleted>>("get-all-deleted");
		}

		public user GetUser(int id)
		{
			return SendNewGet<List<user>>("get-usr", id)[0];
		}

		public void SaveUser(List<user> usr)
		{
			SendNewPost<string>("save-usr", usr.Cast<ITable>().ToList().TimeStampIns().Cast<user>());
		}

		public void UpdateUser(List<user> usr)
		{
			SendNewPost<string>("update-usr", usr.Cast<ITable>().ToList().TimeStampUpd().Cast<user>());
		}

		public void DeleteUser(List<int> id)
		{
			SendNewPost<string>("delete-usr", id);
		}

		public List<order> GetUserOrders(int id_user)
		{
			return SendNewGet<List<order>>("get-usr-orders", id_user);
		}

		public List<order_products> GetAllOrderProduct()
		{
			return SendNewGet<List<order_products>>("get-all-ordr-prods");
		}
		public List<order> SaveOrder(List<order> orders)
		{
			return SendNewPost< List<order>>("save-order", orders.Cast<ITable>().ToList().TimeStampIns().Cast<order>());
		}
		public void UpdateOrder(List<order> orders)
		{
			SendNewPost<string>("update-order", orders.Cast<ITable>().ToList().TimeStampUpd().Cast<order>());
		}

		public List<product> GetAllProducts()
		{
			return SendNewGet<List<product>>("get-all-prods");
		}

		public void DeleteOrder(List<int> ids)
		{
			SendNewPost<string>("delete-order", ids);
		}
		public void SaveProduct(List<product> prods)
		{
			SendNewPost<string>("save-prod", prods.Cast<ITable>().ToList().TimeStampIns().Cast<product>());
		}
		public void DeleteProduct(List<int> ids)
		{
			SendNewPost<string>("delete-prod", ids);
		}

		public List<ViewProduct> GetOrderProducts(int id)
		{
			return SendNewGet<List<ViewProduct>>("get-order-products", id);
		}

		public void SaveOrderProds(List<order_products> prods, int order_id)
		{
			SendNewPost<string>("delete-order-products", new List<int> { order_id });
			SendNewPost<string>("save-order-products", prods.Cast<ITable>().ToList().TimeStampIns().Cast<order_products>());
		}

		public void DeleteOrderProdsOnly(List<int> ids)
		{
			SendNewPost<string>("delete-order-products-only", ids);
		}

		public void SaveOnlyOrderProds(List<order_products> prods)
		{
			SendNewPost<string>("save-order-products", prods.Cast<ITable>().ToList().TimeStampIns().Cast<order_products>());
		}

		public T SendNewPost<T>(string action, object data)
		{
			RestRequest postRequest = new RestRequest("", Method.POST);
			postRequest.AddParameter("action", action); // adds to POST or URL querystring based on Method
			postRequest.AddParameter("data", data.ToJson());
			return client.Execute(postRequest).Content.FromJson<T>();
		}

		public T SendNewGet<T>(string action)
		{
			RestRequest getRequest = new RestRequest("?action=" + action, Method.GET);
			return client.Execute(getRequest).Content.FromJson<T>();
		}

		public T SendNewGet<T>(string action, object data)
		{
			return SendNewGet<T>(action + "&data=" + data.ToJson());
		}
	}
}
