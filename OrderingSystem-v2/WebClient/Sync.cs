using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderingSystem_v2.WebClient;
using OrderingSystem_v2.Database;
using OrderingSystem_v2.Models;
using System.Net;
using OrderingSystem_v2.Utils;
using System.Runtime;
using System.Runtime.InteropServices;
using ServiceStack;
using System.Windows;
using System.Windows.Controls;

namespace OrderingSystem_v2.WebClient
{
	public class Sync
	{
		/*
		ins - insert
		upd - update

		on - online
		of - ofline

		dt - DateTime
		*/
		public Rest rest = new Rest();
		public int user_id = 0;
		public Sync(int usr_id)
		{
			user_id = usr_id;
		}

		public Sync()
		{

		}

		public async Task SyncAll()
		{
			if (user_id == 0) return;

			if (InternetAvailability.IsInternetAvailable())
			{				

				List<deleted> ofDeleted = await Database.GetItemsAsync<deleted>();

				foreach (deleted del in ofDeleted)
				{
					switch (del.table_name)
					{
						case "order":
							await Task.Run(() => rest.DeleteOrder(new List<int> { del.item_id }));
							break;
						case "product":
							await Task.Run(() => rest.DeleteProduct(new List<int> { del.item_id }));
							break;
						case "order_products":
							await Task.Run(() => rest.DeleteOrderProdsOnly(new List<int> { del.item_id }));
							break;
					}
					await Database.DeleteItemAsyncByID<deleted>(del.id);
				}

				List<user> OnUsrs = rest.GetUsers();
				List<user> OfUsrs = await Database.GetNotSyncedItems<user>();

				await SolveConflict(OnUsrs, OfUsrs);

				List<product> OnProds = rest.GetAllProducts();
				List<product> OfProds = await Database.GetNotSyncedItems<product>();

				await SolveConflict(OnProds, OfProds);

				List<order> OnUsrOrds = rest.GetUserOrders(user_id);
				List<order> OfUsrOrds = await Database.GetNotSyncedItems<order>();

				await SolveConflict(OnUsrOrds, OfUsrOrds);

				List<order_products> OnOrdProds = rest.GetAllOrderProduct();
				List<order_products> OfOrdProds = await Database.GetNotSyncedItems<order_products>();

        List<int> OfUpdatedOrdrs = new List<int>();
        foreach(order_products prd in OfOrdProds)
        {
          if (!OfUpdatedOrdrs.Contains(prd.order_id)) {
            OfUpdatedOrdrs.Add(prd.order_id);
          }
        }

        foreach (int ordr_id in OfUpdatedOrdrs) {
          rest.SaveOrderProds(OfOrdProds.Where(o => o.order_id == ordr_id).ToList(), ordr_id);
        }
				


				await Database.ClearDB();

				await Database.SaveItemsAsync<user>(OnUsrs.Cast<ITable>().ToList().SetSyncs(true).Cast<user>().ToList(), true);

				await Database.SaveItemsAsync<product>(OnProds.Cast<ITable>().ToList().SetSyncs(true).Cast<product>().ToList(), true);

				await Database.SaveItemsAsync<order>(OnUsrOrds.Cast<ITable>().ToList().SetSyncs(true).Cast<order>().ToList(), true);

				await Database.SaveItemsAsync<order_products>(OnOrdProds.Cast<ITable>().ToList().SetSyncs(true).Cast<order_products>().ToList(), true);
			}
		}

		/// <summary>
		/// porovná data z webu a z lokálního úložiště a podle posledních úprav data zpracuje
		/// </summary>
		/// <typeparam name="T">class representation of table</typeparam>
		/// <param name="onTable">online table</param>
		/// <param name="ofTable">ofline table</param>
		private async Task SolveConflict<T>(List<T> onTable, List<T> ofTable) where T : ITable, new()
		{
			foreach (T ofItem in ofTable)
			{
				var pom = new T();
				if (pom.timestamp_upd == ofItem.timestamp_upd)
				{
					//insert
					switch (typeof(T).Name)
					{
						case "order":
							var ordrId = rest.SaveOrder(new List<order> { ofItem as order })[0].id;
              try
              {
                var prods = await Database.GetOrderProductsOnly(ofItem.id);
                foreach (order_products prd in prods)
                {
                  prd.order_id = ordrId;
                }
                rest.SaveOrderProds(prods, ordrId);
                await Database.SaveItemsAsync(prods, false);
              }
              catch {
                Console.WriteLine("No order products");
              }
							ofItem.SetSync(true);
							await Database.SaveItemAsync(ofItem as order, false);
              
							break;
						case "product":
							rest.SaveProduct(new List<product> { ofItem as product });
							ofItem.SetSync(true);
							await Database.SaveItemAsync(ofItem as product, false);
							break;
						case "user":
							rest.SaveUser(new List<user> { ofItem as user });
							ofItem.SetSync(true);
							await Database.SaveItemAsync(ofItem as user, false);
							break;                          
					}
				}
				else
				{
					switch (typeof(T).Name)
					{
						case "order":
							rest.UpdateOrder(new List<order> { ofItem as order });
							ofItem.SetSync(true);
							await Database.SaveItemAsync(ofItem as order, false);
							break;
						case "user":
							rest.UpdateUser(new List<user> { ofItem as user });
							ofItem.SetSync(true);
							await Database.SaveItemAsync(ofItem as user, false);
							break;
					}
				}



			}
		}

		/*

		    switch (del.table_name)
			  {
				case "order":
				  break;
				case "product":
				  break;
				case "user":
				  break;
				case "order_products":
				  break;
			  }

		*/



		public Task<List<user>> Login(user usr)
		{
			if (InternetAvailability.IsInternetAvailable())
			{
				return Task<List<user>>.Run(() => rest.Login(usr));

			}
			else
			{
				//db
				try
				{
					var pom = Database.Login(usr);
					return pom;
				}
				catch
				{
					return Task<List<user>>.Run(() => new List<user>());
				}

			}
		}

		public async Task<user> GetUser(int id)
		{
			if (InternetAvailability.IsInternetAvailable())
			{
				//rest
				return await Task<user>.Run(() => rest.GetUser(id));
			}
			else
			{
				//db
				var pom = await Database.GetUsers(id);
				return pom.First();
			}
		}

		public async void SaveUser(List<user> usr)
		{
			if (InternetAvailability.IsInternetAvailable())
			{
				//rest
				rest.SaveUser(usr);
			}
			else
			{
				//db        
				await Database.SaveItemsAsync<user>(usr.Cast<ITable>().ToList().TimeStampIns().SetSyncs(false).Cast<user>().ToList(), true);
			}
		}

		public async void UpdateUser(List<user> usr)
		{
			if (InternetAvailability.IsInternetAvailable())
			{
				//rest
				rest.UpdateUser(usr);
			}
			else
			{
				//db
				await Database.SaveItemsAsync<user>(usr.Cast<ITable>().ToList().TimeStampUpd().SetSyncs(false).Cast<user>().ToList(), false);
			}
		}

		public async Task<List<order>> GetUserOrders(int id_user)
		{
			if (InternetAvailability.IsInternetAvailable())
			{
				//rest
				return rest.GetUserOrders(id_user);
			}
			else
			{
				//db
				var pom = await Database.UsrOrders(id_user);
				return pom;
			}
		}

		public async void SaveOrder(List<order> orders)
		{
			if (InternetAvailability.IsInternetAvailable())
			{
				//rest
				rest.SaveOrder(orders);
			}
			else
			{
				//db
				await Database.SaveItemsAsync<order>(orders.Cast<ITable>().ToList().TimeStampIns().SetSyncs(false).Cast<order>().ToList(), true);
			}
		}

		public async void UpdateOrder(List<order> orders)
		{
			if (InternetAvailability.IsInternetAvailable())
			{
				//rest
				rest.UpdateOrder(orders);
			}
			else
			{
				//db
				await Database.SaveItemsAsync<order>(orders.Cast<ITable>().ToList().TimeStampUpd().SetSyncs(false).Cast<order>().ToList(), false);
			}
		}

		public async Task<List<product>> GetAllProducts()
		{
			if (InternetAvailability.IsInternetAvailable())
			{
				//rest
				return rest.GetAllProducts();
			}
			else
			{
				//db
				var pom = await Database.GetItemsAsync<product>();
				return pom;
			}
		}

		public void DeleteOrder(List<int> ids)
		{
			if (InternetAvailability.IsInternetAvailable())
			{
				//rest
				rest.DeleteOrder(ids);
			}
			else
			{
				//db
				Database.DeleteItemsAsyncByID<order>(ids);
				Database.DeleteOrderProducts(ids);
			}
		}

		public async void SaveProduct(List<product> prods)
		{
			if (InternetAvailability.IsInternetAvailable())
			{
				//rest
				rest.SaveProduct(prods);
			}
			else
			{
				//db
				await Database.SaveItemsAsync<product>(prods.Cast<ITable>().ToList().TimeStampIns().SetSyncs(false).Cast<product>().ToList(), true);
			}
		}

		public void DeleteProduct(List<int> ids)
		{
			if (InternetAvailability.IsInternetAvailable())
			{
				//rest
				rest.DeleteProduct(ids);
			}
			else
			{
				//db
				Database.DeleteItemsAsyncByID<product>(ids);
				Database.DeleteProductOrderProducts(ids);
			}
		}

		public Task<List<ViewProduct>> GetOrderProducts(int id)
		{
			if (InternetAvailability.IsInternetAvailable())
			{
				//rest
				return Task<List<ViewProduct>>.Run(() => rest.GetOrderProducts(id));
			}
			else
			{
				//db
				var pom = Database.GetOrderProducts(id);
				return pom;
			}
		}

		public async void SaveOrderProds(List<order_products> prods, int order_id)
		{
			if (InternetAvailability.IsInternetAvailable())
			{
				//rest
				rest.SaveOrderProds(prods, order_id);
			}
			else
			{
				//db
				Database.DeleteOrderProducts(new List<int> { order_id });
				await Database.SaveItemsAsync<order_products>(prods.Cast<ITable>().ToList().TimeStampIns().SetSyncs(false).Cast<order_products>().ToList(), true);
			}
		}





		public void AlertMe(object text)
		{
			MessageBoxResult result = MessageBox.Show(text.ToJson(), "Confirmation", MessageBoxButton.OK, MessageBoxImage.Warning);
		}

		public bool ApiConnection()
		{
      return CheckConnection("https://student.sps-prosek.cz/~stancst14/api_ordering_system/api.php"); //ping api adresy - příliš pomalé
		}

		public bool CheckConnection(String URL)
		{
			try
			{
				HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
				request.Timeout = 4000;
				request.Credentials = CredentialCache.DefaultNetworkCredentials;
				HttpWebResponse response = (HttpWebResponse)request.GetResponse();

				if (response.StatusCode == HttpStatusCode.OK)
					return true;
				else
					return false;
			}
			catch
			{
				return false;
			}
		}

		private static ItemDatabase _database;
		//instance databáze
		public static ItemDatabase Database
		{
			get
			{
				if (_database == null)
				{
					var fileHelper = new FileHelper();
					_database = new ItemDatabase(fileHelper.GetLocalFilePath("SQLite.db3"));
				}
				return _database;
			}
		}


	}	
}

