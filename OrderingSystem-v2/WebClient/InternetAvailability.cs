using System.Runtime.InteropServices;

namespace OrderingSystem_v2.WebClient
{
  public static class InternetAvailability
  {
    [DllImport("wininet.dll")]
    private extern static bool InternetGetConnectedState(out int description, int reservedValue);
    /// <summary>
    /// má windows připojení k inetnetu?
    /// lepčí než ping jelikož je to rychlejší
    /// </summary>
    /// <returns></returns>
    public static bool IsInternetAvailable()
    {
      int description;
      return InternetGetConnectedState(out description, 0);
      //return false;
    }
  }
}
