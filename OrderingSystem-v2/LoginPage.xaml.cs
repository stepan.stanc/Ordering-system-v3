using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OrderingSystem_v2.Models;
using OrderingSystem_v2.WebClient;
using OrderingSystem_v2.Utils;

namespace OrderingSystem_v2
{
  /// <summary>
  /// Interaction logic for LoginPage.xaml
  /// </summary>
  public partial class LoginPage : Page
  {
    public Rest api = new Rest();
    public Sync sync = new Sync();
    public LoginPage()
    {
      InitializeComponent();
    }
    private async void loginBtn_Click(object sender, RoutedEventArgs e)
    {
      var usrnm = inName.Text;
      var pass = inPass.Password.EncodeSHA256();
      if (string.IsNullOrWhiteSpace(pass) || string.IsNullOrWhiteSpace(usrnm)) return;
      var usr = await sync.Login(new user { username = usrnm, password = pass });
      if (usr == null || usr.Count == 0) return;
      App.Current.MainWindow.Content = new MainPage(usr.First().id);
    }
    private void singupBtn_Click(object sender, RoutedEventArgs e)
    {
      var nm = name.Text;
      var snm = surname.Text;
      var brth = Convert.ToDateTime(birth.Text);
      var unm = upName.Text;
      var ups = upPass.Password.EncodeSHA256();
      var rep = repPass.Password.EncodeSHA256();
      if (ups != rep && !string.IsNullOrWhiteSpace(unm) && !string.IsNullOrWhiteSpace(ups)) return;
      sync.SaveUser(new List<user> { new user { name = nm, surname = snm, birthdate = user.ToBirthdayFormat(brth), password = ups, username = unm } });
      inTab.IsSelected = true;
    }    
  }
}
