using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderingSystem_v2.WebClient;
using OrderingSystem_v2.Database;
using SQLite;

namespace OrderingSystem_v2.Models
{
  public class ViewOrder : order
  {
    private Rest api = new Rest();
    private Sync sync = new Sync();    

    public string products { get; set; }
    /*

       var prods = await sync.GetOrderProducts(id);
        var str = "";
        foreach (ViewProduct prod in prods)
        {
          str += prod.count + "x " + prod.name + ", ";
          prc += prod.fullPrice;
        }
        return str;
        */

    public long fullPrice { get; set; }

    public override string ToString()
    {
      return "Order date : " + datetime.ToString();
    }
  }
}
