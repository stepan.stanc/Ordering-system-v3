using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderingSystem_v2.Database;
using SQLite;
using OrderingSystem_v2.Utils;

namespace OrderingSystem_v2.Models
{
	public class user : ITable
	{
		public int id { get; set; }
		[PrimaryKey, AutoIncrement]
		public int lokalId { get; set; }
		public string name { get; set; }
		public string surname { get; set; }
		public string username { get; set; }
		public string birthdate { get; set; }
		public string password { get; set; }
		public string timestamp_ins { get; set; } = new DateTime().ToPHPDatetime();
		public string timestamp_upd { get; set; } = new DateTime().ToPHPDatetime();
		public bool synced { get; set; }

		public static string ToBirthdayFormat(DateTime date)
		{
			return date.ToString("yyyy-MM-dd");
		}

		public override string ToString()
		{
			return "username: " + username;
		}
	}
}
