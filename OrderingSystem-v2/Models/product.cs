using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderingSystem_v2.Database;
using SQLite;
using OrderingSystem_v2.Utils;

namespace OrderingSystem_v2.Models
{
	public class product : ITable
	{
		public int id { get; set; }
		[PrimaryKey, AutoIncrement]
		public int lokalId { get; set; }
		public string name { get; set; }
		public double price { get; set; }
		public string description { get; set; }
		public string timestamp_ins { get; set; } = new DateTime().ToPHPDatetime();
		public string timestamp_upd { get; set; } = new DateTime().ToPHPDatetime();
		public bool synced { get; set; }

		public override string ToString()
		{
			return id + " name: " + name + ", price: " + price;
		}
	}
}
