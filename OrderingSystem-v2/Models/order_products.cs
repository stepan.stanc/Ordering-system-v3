using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderingSystem_v2.Database;
using SQLite;
using OrderingSystem_v2.Utils;

namespace OrderingSystem_v2.Models
{
	public class order_products : ITable
	{
		public int id { get; set; }
		[PrimaryKey, AutoIncrement]
		public int lokalId { get; set; }
		public int order_id { get; set; }
		public int product_id { get; set; }
		public int count { get; set; }
		public string timestamp_ins { get; set; } = new DateTime().ToPHPDatetime();
		public string timestamp_upd { get; set; } = new DateTime().ToPHPDatetime();
		public bool synced { get; set; }
	}
}
