using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderingSystem_v2.Database;
using SQLite;
using OrderingSystem_v2.Utils;

namespace OrderingSystem_v2.Models
{
	public class order : ITable
	{
		public int id { get; set; }
		[PrimaryKey, AutoIncrement]
		public int lokalId { get; set; }
		public int user_id { get; set; }
		public string datetime { get; set; }
		public bool ordered { get; set; }
		public bool hidden { get; set; }
		public string timestamp_ins { get; set; } = new DateTime().ToPHPDatetime();
		public string timestamp_upd { get; set; } = new DateTime().ToPHPDatetime();
		public bool synced { get; set; }

		public override string ToString()
		{
			return "Order date : " + datetime.ToString();
		}
	}
}
